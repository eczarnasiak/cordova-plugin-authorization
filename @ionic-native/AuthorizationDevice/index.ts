import { Injectable } from '@angular/core';
import { Cordova, IonicNativePlugin, Plugin } from '@ionic-native/core';

/**
 * @name AuthorizationDevice
 * @description
 * authorization-device
 *
 * @interfaces
 * AuthorizationDeviceStartupOptions
 */

export interface AuthorizationDevice_INTERFACE_OPTIONS {
  option?: any;
}

@Plugin({
  pluginName: 'AuthorizationDevice',
  plugin: 'cordova-plugin-authorization-device',
  pluginRef: 'AuthorizationDevice',
  repo: 'REPO',
  platforms: ['iOS,Android'],
})
@Injectable()
export class AuthorizationDevice extends IonicNativePlugin {
  /**
   * Starts AuthorizationDevice.
   * @param {AuthorizationDevice_INTERFACE_OPTIONS} options
   * @return {Promise<any>}
   */
  @Cordova({
    successIndex: 1,
    errorIndex: 2,
  })
  startPlugin(options: PLUGIN_INTERFACE_OPTIONS): Promise<any> {
    return;
  }
}
